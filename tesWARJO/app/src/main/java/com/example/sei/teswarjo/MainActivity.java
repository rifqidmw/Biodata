package com.example.sei.teswarjo;

import android.content.Intent;
import android.hardware.SensorManager;
import android.location.LocationListener;
import android.net.Uri;
import android.widget.Toast;

import com.example.sei.teswarjo.ARview.AbstractArchitectCamActivity;
import com.example.sei.teswarjo.ARview.ArchitectViewHolderInterface;
import com.example.sei.teswarjo.ARview.LocationProvider;
import com.example.sei.teswarjo.ARview.PoiDetailActivity;
import com.example.sei.teswarjo.ARview.WikitudeSDKConstants;
import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;

public class MainActivity extends AbstractArchitectCamActivity {

    private long lastCalibrationToastShownTimeMillis = System.currentTimeMillis();

    @Override
    protected StartupConfiguration.CameraPosition getCameraPosition() {
        return StartupConfiguration.CameraPosition.DEFAULT;
    }

    @Override
    protected boolean hasGeo() {
        return true;
    }

    @Override
    protected boolean hasIR() {
        return false;
    }

    @Override
    public String getActivityTitle() {
        return "";
    }

    @Override
    public String getARchitectWorldPath() {
        return "teswarjo/wisataalam/index.html";
    }

    @Override
    public ArchitectView.ArchitectUrlListener getUrlListener() {
        return new ArchitectView.ArchitectUrlListener() {
            public boolean urlWasInvoked(String paramAnonymousString) {
                Uri localUri = Uri.parse(paramAnonymousString);
                if ("markerselected".equalsIgnoreCase(localUri.getHost())) {
                    Intent localIntent = new Intent(MainActivity.this, PoiDetailActivity.class);
                    localIntent.putExtra("id", String.valueOf(localUri.getQueryParameter("id")));
                    localIntent.putExtra("title", String.valueOf(localUri.getQueryParameter("title")));
                    localIntent.putExtra("address", String.valueOf(localUri.getQueryParameter("address")));
                    localIntent.putExtra("phone", String.valueOf(localUri.getQueryParameter("phone")));
                    localIntent.putExtra("description", String.valueOf(localUri.getQueryParameter("description")));
                    localIntent.putExtra("latitude", Double.valueOf(localUri.getQueryParameter("latitude")));
                    localIntent.putExtra("longitude", Double.valueOf(localUri.getQueryParameter("longitude")));
                    localIntent.putExtra("img", String.valueOf(localUri.getQueryParameter("img")));
                    localIntent.putExtra("img1", String.valueOf(localUri.getQueryParameter("img1")));
                    localIntent.putExtra("img2", String.valueOf(localUri.getQueryParameter("img2")));
                    localIntent.putExtra("img3", String.valueOf(localUri.getQueryParameter("img3")));
                    localIntent.putExtra("img4", String.valueOf(localUri.getQueryParameter("img4")));
                    localIntent.putExtra("img5", String.valueOf(localUri.getQueryParameter("img5")));
                    MainActivity.this.startActivity(localIntent);
                }
                return true;
            }
        };
    }



    @Override
    public int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    public String getWikitudeSDKLicenseKey() {
        return WikitudeSDKConstants.WIKITUDE_SDK_KEY;
    }

    @Override
    public int getArchitectViewId() {
        return R.id.architectView;
    }

    @Override
    public ILocationProvider getLocationProvider(LocationListener locationListener) {
        return new LocationProvider(this, locationListener);
    }






    @Override
    public ArchitectView.SensorAccuracyChangeListener getSensorAccuracyListener() {
        return new ArchitectView.SensorAccuracyChangeListener() {
            @Override
            public void onCompassAccuracyChanged( int accuracy ) {
				/* UNRELIABLE = 0, LOW = 1, MEDIUM = 2, HIGH = 3 */
                if ( accuracy < SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM && MainActivity.this != null && !MainActivity.this.isFinishing() && System.currentTimeMillis() - MainActivity.this.lastCalibrationToastShownTimeMillis > 5 * 1000) {
                    Toast.makeText(MainActivity.this, R.string.compass_accuracy_low, Toast.LENGTH_LONG).show();
                    MainActivity.this.lastCalibrationToastShownTimeMillis = System.currentTimeMillis();
                }
            }
        };
    }

    @Override
    public float getInitialCullingDistanceMeters() {
        return ArchitectViewHolderInterface.CULLING_DISTANCE_DEFAULT_METERS;

    }

}
