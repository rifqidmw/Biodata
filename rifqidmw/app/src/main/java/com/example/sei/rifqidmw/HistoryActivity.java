package com.example.sei.rifqidmw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TabHost;

public class HistoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        TabHost tabHost=(TabHost)findViewById(R.id.tab_host);
        tabHost.setup();

        TabHost.TabSpec spec1=tabHost.newTabSpec("Pendidikan");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Pendidikan");

        TabHost.TabSpec spec2=tabHost.newTabSpec("Organisasi");
        spec2.setIndicator("Organisasi");
        spec2.setContent(R.id.tab2);

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
    }
}
